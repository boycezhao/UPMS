// JavaScript Document
//支持Enter键登录
document.onkeydown = function(e){
	if($(".bac").length==0)
	{
		if(!e) e = window.event;
		if((e.keyCode || e.which) == 13){
			var obtnLogin=document.getElementById("submit_btn")
			obtnLogin.focus();
		}
	}
}

$(function(){
	//提交表单
	$('#submit_btn').click(function(){
		show_loading();
		if($('#userName').val() == ''){
			show_err_msg('用户名还没填呢！');	
			$('#userName').focus();
		}else if($('#password').val() == ''){
			show_err_msg('密码还没填呢！');
			$('#password').focus();
		}else{
			$.ajax({
				url : Utils.basePath +"login",
				data: $("#form").serialize(),
				method : "post",
				dataType : "json",
				success : function(data) {
					if (data.status == 200) {
						show_msg('登录成功咯！  正在为您跳转...',Utils.basePath +'home');	
					}else{
						show_err_msg(data.errorInfo);
						return false;
					}
				}
			})
		}
	});
	
	//图片切换
	$.supersized({

	    // Functionality
	    slide_interval     : 4000,    // Length between transitions
	    transition         : 1,    // 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
	    transition_speed   : 1000,    // Speed of transition
	    performance        : 1,    // 0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)

	    // Size & Position
	    min_width          : 0,    // Min width allowed (in pixels)
	    min_height         : 0,    // Min height allowed (in pixels)
	    vertical_center    : 1,    // Vertically center background
	    horizontal_center  : 1,    // Horizontally center background
	    fit_always         : 0,    // Image will never exceed browser width or height (Ignores min. dimensions)
	    fit_portrait       : 1,    // Portrait images will not exceed browser height
	    fit_landscape      : 0,    // Landscape images will not exceed browser width

	    // Components
	    slide_links        : 'blank',    // Individual links for each slide (Options: false, 'num', 'name', 'blank')
		slides : [ // Slideshow Images
		{   //image : '../img/backgrounds/0.jpg'
			image : Utils.basePath + 'static/img/backgrounds/0.jpg'
		}, {
			image : Utils.basePath + 'static/img/backgrounds/1.jpg'
		}, {
			image : Utils.basePath + 'static/img/backgrounds/2.jpg'
		}, {
			image : Utils.basePath + 'static/img/backgrounds/3.jpg'
		} ]

	});
	
});